package ee.bcs.valiit.bank.controller;

import ee.bcs.valiit.bank.data.Customer;
import ee.bcs.valiit.bank.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<Customer> list() {
        log.debug("Customer list called ...");
        List<Customer> list = customerService.list();
        list.sort((o1, o2) -> {
            if (o1.getId().compareTo(o2.getId()) > 0) {
                return 1;
            } else if (o1.getId().compareTo(o2.getId()) < 0) {
                return -1;
            }
            return 0;
        });
        return list;
    }

    @GetMapping(value = "/get/{id}", produces = "application/json")
    public Customer get(@PathVariable Long id) {
        return customerService.get(id);
    }

    @PostMapping(value = "/delete/{id}", produces = "application/json")
    public void delete(@PathVariable  Long id) {
        Customer customer = customerService.get(id);
        customerService.delete(customer);
    }

    @PostMapping(value = "/save")
    public void save(@RequestBody Customer customer) {
        Customer dbCustomer = customerService.get(customer.getId());
        dbCustomer.setFirstname(customer.getFirstname());
        dbCustomer.setSurname(customer.getSurname());
        dbCustomer.setBirthday(customer.getBirthday());
        dbCustomer.setAddress(customer.getAddress());
        customerService.save(dbCustomer);
    }

    @PostMapping(value = "/add")
    public void add(@RequestBody Customer customer) {
        customerService.save(customer);
    }

}
