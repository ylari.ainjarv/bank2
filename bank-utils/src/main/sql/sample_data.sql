-- insert users and roles
DO $$
  DECLARE
    bank_user_id BIGINT;
    bank_role_id BIGINT;
  BEGIN
    INSERT INTO role(name) VALUES('bank.user') RETURNING id INTO bank_role_id;
    INSERT INTO users(name, email, username, password)
      VALUES('Ülari Ainjärv', 'ylari@digipurk.ee', 'ylari', '28c5d63c80cfa3a5f2d4d3033937963c467ebaee648b8dcf9c608af893dcd178')
      RETURNING id INTO bank_user_id;
    INSERT INTO user_role(user_id, role_id) VALUES(bank_user_id, bank_role_id);

    INSERT INTO role(name) VALUES('bank.admin') RETURNING id INTO bank_role_id;
    INSERT INTO users(name, email, username, password)
      VALUES('Administrator', 'admin@digipurk.ee', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918')
      RETURNING id INTO bank_user_id;
    INSERT INTO user_role(user_id, role_id) VALUES(bank_user_id, bank_role_id);
  END
$$;
